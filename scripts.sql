
drop table student;
create table student(
id SERIAL PRIMARY KEY,
first_name varchar(45),
last_name varchar(45),
email varchar(45)
);

create table customer(
id  int PRIMARY KEY,
first_name varchar(45),
last_name varchar(45),
email varchar(45)
);

create table instructor(
id  SERIAL PRIMARY KEY,
first_name varchar(45),
last_name varchar(45),
email varchar(45),
instructor_detail_id int references instructor_detail(id)
);

create table instructor_detail(
id  SERIAL PRIMARY KEY,
youtube_channel varchar(128),
hobby varchar(45)
);

CREATE SEQUENCE cust_seq START 100;

select nextval('cust_seq')

drop table Course;
create table Course(
id  SERIAL PRIMARY KEY,
title varchar(45) unique,
instructor_id int references instructor(id)
);


create table Review(
id  SERIAL PRIMARY KEY,
comment varchar(45) ,
course_id int references Course(id)
);

create table course_student(
course_id int references course(id),
student_id int references student(id),
primary key(course_id,student_id)
);