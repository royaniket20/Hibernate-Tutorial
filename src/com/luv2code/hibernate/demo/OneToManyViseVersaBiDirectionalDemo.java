package com.luv2code.hibernate.demo;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Course;

public class OneToManyViseVersaBiDirectionalDemo {
public static void main(String[] args) {
	//create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(InstructorDetail.class).addAnnotatedClass(Instructor.class).addAnnotatedClass(Course.class).buildSessionFactory();
		//get sessioin
		Session session = factory.getCurrentSession();
		try {
			//create new student object
			int tempId=4;
			//begin transaction
			     session = factory.getCurrentSession();
				session.beginTransaction();
				Instructor instructor = session.get(Instructor.class, tempId);
				
				 if(instructor!=null) {
					 System.out.println("-----instructor-----"+instructor.toString());
						 System.out.println("===course==="+instructor.getCourses());
				 }else {
					System.out.println("No such data present");
				 }
				 
				 int tempCourseid = 1;
				 Course course = session.get(Course.class, tempCourseid);
				 System.out.println(" this course ----"+course.toString());
				 System.out.println("====related instructor - "+course.getInstructor().toString());
				//System.out.println("----deleting this course------");
				//session.delete(course);
				 session.getTransaction().commit();
				
			   factory.close();
			System.out.println("*done***********");
		} catch (Exception e) {
		e.printStackTrace();
		}
	
}
}

