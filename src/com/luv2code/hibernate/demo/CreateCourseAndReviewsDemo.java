package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Review;
import com.luv2code.hibernate.demo.entity.Student;

public class CreateCourseAndReviewsDemo {
public static void main(String[] args) {
	//create session factory
	SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Instructor.class).addAnnotatedClass(InstructorDetail.class).addAnnotatedClass(Course.class).addAnnotatedClass(Review.class).buildSessionFactory();
		//get sessioin
	Session session = factory.getCurrentSession();
	try {
		session = factory.getCurrentSession();
		session.beginTransaction();
		
	
		Course course = new Course(".NET");
		course.addReview(new Review("holy fun!!1"));
		course.addReview(new Review("holy fun!!2"));
		course.addReview(new Review("holy fun!!3"));
		course.addReview(new Review("holy fun!!4"));
		
           session.save(course);
			session.getTransaction().commit();
			System.out.println("-----called-----");
			System.out.println("Couser : "+course);
			System.out.println("Reviews : "+course.getReviews());
		factory.close();
		System.out.println("*done***********");
	} catch (Exception e) {
	e.printStackTrace();
	}
}
}
