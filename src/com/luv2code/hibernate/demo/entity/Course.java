package com.luv2code.hibernate.demo.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * create table Course(
id  SERIAL PRIMARY KEY,
title varchar(45) unique,
instructor_id int references instructor(id)
);
 * @author royan
 *
 */
@Entity
@Table(name="Course")
public class Course {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name="id")
private int id;
@Column(name="title")
private String title;


public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}

@Override
public String toString() {
	return "Course [id=" + id + ", title=" + title + "]";
}
public Course() {
	super();
	// TODO Auto-generated constructor stub
}
public Course( String title) {
	super();
	this.title = title;
}

@ManyToOne(cascade= {CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REFRESH,CascadeType.DETACH})
@JoinColumn(name="instructor_id")
Instructor Myinstructor;
public Instructor getInstructor() {
	return Myinstructor;
}
public void setInstructor(Instructor Myinstructor) {
	this.Myinstructor = Myinstructor;
}

@OneToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
@JoinColumn(name="course_id")
private List<Review> reviews;


@ManyToMany(fetch=FetchType.LAZY,cascade= {CascadeType.PERSIST,CascadeType.MERGE,CascadeType.REFRESH,CascadeType.DETACH})
@JoinTable(name="course_student",
joinColumns=@JoinColumn(name="course_id"),
inverseJoinColumns=@JoinColumn(name="student_id"))
private List<Student> students;

public List<Review> getReviews() {
	return reviews;
}
public void setReviews(List<Review> reviews) {
	this.reviews = reviews;
}

public void addReview(Review review)
{
	if(reviews==null)
		reviews = new ArrayList<>();
	reviews.add(review);
}
public List<Student> getStudents() {
	return students;
}
public void setStudents(List<Student> students) {
	this.students = students;
}

public void addStudent(Student student) {
	if(students==null)
		students = new ArrayList<>();
	students.add(student);

}


}
