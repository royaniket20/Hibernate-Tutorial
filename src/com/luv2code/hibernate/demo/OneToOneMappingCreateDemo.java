package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;

public class OneToOneMappingCreateDemo {
public static void main(String[] args) {
	//create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Instructor.class).addAnnotatedClass(InstructorDetail.class).addAnnotatedClass(Course.class).buildSessionFactory();
		//get sessioin
		Session session = factory.getCurrentSession();
		try {
			//create new student object
			Instructor instructor = new Instructor("Sriparna", "Roy", "royaniket20@gmail.com");
		    InstructorDetail instructorDetail = new InstructorDetail("www.google.com", "gaming");
		    instructor.setInstructorDetails(instructorDetail);
			//begin transaction
		    System.out.println("-----called-----"+instructor);
			     session = factory.getCurrentSession();
				session.beginTransaction();
	             session.save(instructor);
				session.getTransaction().commit();
				
			   factory.close();
			System.out.println("*done***********");
		} catch (Exception e) {
		e.printStackTrace();
		}
}
}
