package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Review;
import com.luv2code.hibernate.demo.entity.Student;

public class GetCourseAndReviewsDemo {
public static void main(String[] args) {
	//create session factory
	SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Instructor.class).addAnnotatedClass(InstructorDetail.class).addAnnotatedClass(Course.class).addAnnotatedClass(Review.class).buildSessionFactory();
		//get sessioin
	Session session = factory.getCurrentSession();
	try {
		session = factory.getCurrentSession();
		session.beginTransaction();
	int id = 4;
	Course course = session.get(Course.class, id);
		
      
		
			System.out.println("-----called-----");
			System.out.println("Couser : "+course);
			System.out.println("Reviews : "+course.getReviews());
			System.out.println("-----deleting the course-------");
			session.delete(course);
			session.getTransaction().commit();
		factory.close();
		System.out.println("*done***********");
	} catch (Exception e) {
	e.printStackTrace();
	}
}
}
