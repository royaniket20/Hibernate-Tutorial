package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Review;
import com.luv2code.hibernate.demo.entity.Student;

public class CreateCourseAndStudentsDemo {
public static void main(String[] args) {
	//create session factory
	SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
			.addAnnotatedClass(Instructor.class)
			.addAnnotatedClass(InstructorDetail.class)
			.addAnnotatedClass(Review.class)
			.addAnnotatedClass(Course.class)
			.addAnnotatedClass(Student.class)
			.buildSessionFactory();
		//get sessioin
	Session session = factory.getCurrentSession();
	try {
		session = factory.getCurrentSession();
		session.beginTransaction();
		Course course = new Course("MY WIFE COURSE");
		System.out.println(" saving the course");
        session.save(course);
	
		Student student1 = new Student("Anik1", "roy1", "xxx@gmail.com1");
		Student student2 = new Student("Anik2", "roy2", "xxx@gmail.com2");
		course.addStudent(student1);
		course.addStudent(student2);
		System.out.println(" saving the student");
		session.save(student1);
		session.save(student2);
		session.getTransaction().commit();
		factory.close();
		System.out.println("*done***********");
	} catch (Exception e) {
	e.printStackTrace();
	}
}
}
