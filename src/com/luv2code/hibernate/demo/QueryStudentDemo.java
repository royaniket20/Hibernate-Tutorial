package com.luv2code.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class QueryStudentDemo {
public static void main(String[] args) {
	//create session factory
	SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();
	//get sessioin
	Session session = factory.getCurrentSession();
	try {
	
		session = factory.getCurrentSession();
			session.beginTransaction();
       // List<Student> students = session.createQuery("from Student").getResultList();
			// List<Student> students = session.createQuery("from Student s where s.firstName = 'Amit'").getResultList();
		//	 List<Student> students = session.createQuery("from Student s where s.firstName = 'Amit' OR s.lastName = 'Roy'").getResultList();
			 List<Student> students = session.createQuery("from Student s where s.email LIKE '%gmail.com'").getResultList();
			for(Student student : students)
        	System.out.println("****"+student);
			
			
			session.getTransaction().commit();
			
		factory.close();
		
	} catch (Exception e) {
	e.printStackTrace();
	}
}
}
