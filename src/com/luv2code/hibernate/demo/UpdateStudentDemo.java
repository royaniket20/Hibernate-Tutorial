package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.google.gson.Gson;
import com.luv2code.hibernate.demo.entity.Student;

public class UpdateStudentDemo {
public static void main(String[] args) {
	
	//create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();
		//get sessioin
		Session session = factory.getCurrentSession();
		try {
			int Studentid =5;
				 session = factory.getCurrentSession();
				 session.beginTransaction();
				Student std = session.get(Student.class,Studentid);
				std.setFirstName("Sriparna");
				std.setLastName("Samanta Roy");
				session.getTransaction().commit();
				
				
				 session = factory.getCurrentSession();
				 session.beginTransaction();
			     session.createQuery("UPDATE Student set email='foo@gmail.com'").executeUpdate();
				 
				session.getTransaction().commit();
				factory.close();
			System.out.println("*done***********");
		} catch (Exception e) {
		e.printStackTrace();
		}
	
	
}
}
