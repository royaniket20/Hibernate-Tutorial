package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Student;

public class CreateCourseDemo {
public static void main(String[] args) {
	//create session factory
	SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Instructor.class).addAnnotatedClass(InstructorDetail.class).addAnnotatedClass(Course.class).buildSessionFactory();
		//get sessioin
	Session session = factory.getCurrentSession();
	try {
		session = factory.getCurrentSession();
		session.beginTransaction();
		int tempId = 4;
		Instructor instructor = session.get(Instructor.class, tempId);
		//create new student object
		Course course = new Course(".NET");
		instructor.add(course);
		//begin transaction       	
           session.save(course);
			session.getTransaction().commit();
			System.out.println("-----called-----");
		factory.close();
		System.out.println("*done***********");
	} catch (Exception e) {
	e.printStackTrace();
	}
}
}
