package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class CreateStudentDemo {
public static void main(String[] args) {
	//create session factory
	SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();
	//get sessioin
	Session session = factory.getCurrentSession();
	try {
		//create new student object
		Student student = new Student("Aniket", "Roy", "royaniket20@gmail.com");
		//begin transaction
	
		session = factory.getCurrentSession();
			session.beginTransaction();
           session.save(student);
			session.getTransaction().commit();
			System.out.println("-----called-----");
		factory.close();
		System.out.println("*done***********");
	} catch (Exception e) {
	e.printStackTrace();
	}
}
}
