package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Student;

public class PrimaryKeyDemo {

	
	public static void main(String[] args) {
		//create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();
		//get sessioin
		Session session = factory.getCurrentSession();
		try {
			//create new student object
			Student student1 = new Student("Aniket", "Roy", "royaniket20@gmail.com");
			Student student2 = new Student("Aniket", "Paul", "Paulaniket20@gmail.com");
			Student student3 = new Student("Aniket", "Mukharjee", "Mukharjeeaniket20@gmail.com");
			//begin transaction
		
			session = factory.getCurrentSession();
				session.beginTransaction();
	           session.save(student1);
	           session.save(student2);
	           session.save(student3);
				session.getTransaction().commit();
				System.out.println("-----called-----");
			factory.close();
			System.out.println("*done***********");
		} catch (Exception e) {
		e.printStackTrace();
		}
	}
}
