package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Course;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Review;
import com.luv2code.hibernate.demo.entity.Student;

public class AddCoursesToStudentDemo {
public static void main(String[] args) {
	//create session factory
	SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
			.addAnnotatedClass(Instructor.class)
			.addAnnotatedClass(InstructorDetail.class)
			.addAnnotatedClass(Review.class)
			.addAnnotatedClass(Course.class)
			.addAnnotatedClass(Student.class)
			.buildSessionFactory();
		//get sessioin
	Session session = factory.getCurrentSession();
	try {
		session = factory.getCurrentSession();
		session.beginTransaction();
		int StudId = 24;
		Student student = session.get(Student.class, StudId);
		System.out.println("Student detail : "+student.toString());
		System.out.println("enrolled course : "+student.getCourses());
		Course course1 = new Course("MY WIFE COURSE1");
		Course course2 = new Course("MY WIFE COURSE2");
		Course course3 = new Course("MY WIFE COURSE3");
		Course course4 = new Course("MY WIFE COURSE4");
		System.out.println(" saving the course");
		course1.addStudent(student);
		course2.addStudent(student);
		course3.addStudent(student);
		course4.addStudent(student);
		
        session.save(course1);
        session.save(course2);
        session.save(course3);
        session.save(course4);
        
		session.getTransaction().commit();
		factory.close();
		System.out.println("*done***********");
	} catch (Exception e) {
	e.printStackTrace();
	}
}
}
