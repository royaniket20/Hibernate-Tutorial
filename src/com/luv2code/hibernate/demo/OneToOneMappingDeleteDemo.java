package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Customer;
import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Student;

public class OneToOneMappingDeleteDemo {
public static void main(String[] args) {
	//create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Instructor.class).addAnnotatedClass(InstructorDetail.class).buildSessionFactory();
		//get sessioin
		Session session = factory.getCurrentSession();
		try {
			//create new student object
			int tempId=1;
			//begin transaction
			     session = factory.getCurrentSession();
				session.beginTransaction();
				Instructor instructor = session.get(Instructor.class, tempId);
				 System.out.println("-----called-----"+instructor);
				 if(instructor!=null) {
	             System.out.println("Delete the instructor-----------");
					 session.delete(instructor);
				 }else {
					System.out.println("No such data present");
				 }
				session.getTransaction().commit();
				
			   factory.close();
			System.out.println("*done***********");
		} catch (Exception e) {
		e.printStackTrace();
		}
	
}
}
