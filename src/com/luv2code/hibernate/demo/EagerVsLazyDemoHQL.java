package com.luv2code.hibernate.demo;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import com.luv2code.hibernate.demo.entity.Instructor;
import com.luv2code.hibernate.demo.entity.InstructorDetail;
import com.luv2code.hibernate.demo.entity.Course;

public class EagerVsLazyDemoHQL {
public static void main(String[] args) {
	//create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(InstructorDetail.class).addAnnotatedClass(Instructor.class).addAnnotatedClass(Course.class).buildSessionFactory();
		//get sessioin
		Session session = factory.getCurrentSession();
		try {
			//create new student object
			int tempId=4;
			//begin transaction
			     session = factory.getCurrentSession();
				session.beginTransaction();
				Query<Instructor> query = session.createQuery("select i from Instructor i JOIN FETCH i.courses where i.id=:xxx");
				query.setParameter("xxx", tempId);
				Instructor instructor = query.getSingleResult();
				
				 System.out.println("-----instructor-----"+instructor.toString());
			
				 session.getTransaction().commit();
				session.close();
			   factory.close();
			   System.out.println("session is now closed---");
			   System.out.println("===course==="+instructor.getCourses());//FOR EAGER THIS WILL WORK
			System.out.println("*done***********");
		} catch (Exception e) {
		e.printStackTrace();
		}
	
}
}

