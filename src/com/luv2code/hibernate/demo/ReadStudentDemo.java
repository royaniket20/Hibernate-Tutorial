package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.google.gson.Gson;
import com.luv2code.hibernate.demo.entity.Student;

public class ReadStudentDemo {
public static void main(String[] args) {
	//create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class).buildSessionFactory();
		//get sessioin
		Session session = factory.getCurrentSession();
		try {
			//create new student object
			Student student = new Student("Amit", "Roy", "royaniket20@gmail.com");
			//begin transaction
			session = factory.getCurrentSession();
				session.beginTransaction();
	           session.save(student);
				session.getTransaction().commit();
				System.out.println("-----called-----");
				// retrive the  data
				System.out.println("----generated stident id -----"+student.getId());
				 session = factory.getCurrentSession();
				 session.beginTransaction();
				System.out.println("===retrived student===="+new Gson().toJson(session.get(Student.class, student.getId())));
				session.getTransaction().commit();
				
				factory.close();
			System.out.println("*done***********");
		} catch (Exception e) {
		e.printStackTrace();
		}
	
}
}
