package com.luv2code.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.luv2code.hibernate.demo.entity.Customer;
import com.luv2code.hibernate.demo.entity.Student;

public class CreateCustomerDemo {
public static void main(String[] args) {
	//create session factory
	SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Customer.class).buildSessionFactory();
	//get sessioin
	Session session = factory.getCurrentSession();
	try {
		//create new student object
		Customer student1 = new Customer("Aniket1", "Roy", "royaniket20@gmail.com");
		Customer student2 = new Customer("Aniket2", "Roy", "royaniket20@gmail.com");
		Customer student3 = new Customer("Aniket3", "Roy", "royaniket20@gmail.com");
		Customer student4 = new Customer("Aniket4", "Roy", "royaniket20@gmail.com");
		//begin transaction
	
		session = factory.getCurrentSession();
			session.beginTransaction();
           session.save(student1);
           session.save(student2);
           session.save(student3);
           session.save(student4);
			session.getTransaction().commit();
			System.out.println("-----called-----");
		factory.close();
		System.out.println("*done***********");
	} catch (Exception e) {
	e.printStackTrace();
	}
}
}
