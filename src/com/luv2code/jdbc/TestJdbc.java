package com.luv2code.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class TestJdbc {

	public static void main(String[] args) {
		String jdbcUrl = "jdbc:postgresql://localhost:5432/postgres";
		String userName ="postgres";
		String passWord="postgres";
		String driver = "org.postgresql.Driver";
		try {
		System.out.println("Connecting to the Database : "+jdbcUrl);
		Class.forName(driver);
		Connection connection = DriverManager.getConnection(jdbcUrl, userName, passWord);
		System.out.println("Connection Successful !!!"+connection.isClosed());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
